extends Control


const Icon: PackedScene = preload("res://source/scene/Icon.tscn")


enum { UP, DOWN }

var props: Dictionary # свойства уровня. приходит из Game
var textures: Array = [] # текстуры уровня. приходит из Game
var direction: int # направление движения. приходит из Game

var rows: float = 15.0 # количество иконок в столбце
var death_offset: float # дистанция, после которой удаляется иконка

var height: float # высота колонки

var is_spin_active: bool = false
var icon_max_speed: int = 3000


# BUILTINS - - - - - - - - -


func _ready() -> void:
	height = get_rect().size.y
	if direction == UP:
		death_offset = 0 - (props.h + props.offset_y) * (rows - props.count_y - 1)
	elif direction == DOWN:
		death_offset = (props.h + props.offset_y) * (rows - 1)

	for i in rows:
		var icon: TextureRect = create_icon(height, death_offset)
		if direction == UP:
			icon.set_position(Vector2(0.0, (props.h + props.offset_y) * i))
		elif direction == DOWN:
			icon.set_position(Vector2(0.0, height - props.h * (i + 1) - props.offset_y * i))
		self.add_child(icon)


# METHODS - - - - - - - - -


func create_icon(column_h: float, ic_death_offset: float) -> TextureRect:
	var icon: TextureRect = Icon.instance() as TextureRect
	icon.texture = textures[rand_range(0, textures.size())]
	icon.set("column_h", column_h)
	icon.set("direction", direction)
	icon.set("death_offset", ic_death_offset)
	icon.set("max_speed", icon_max_speed)
	icon.set("offset_y", props.offset_y)
	var _imma_head_out = icon.connect("imma_head_out", self, "_on_Icon_imma_head_out")
	return icon


func move_icons(delay: float) -> void:
	yield(get_tree().create_timer(delay), "timeout")
	is_spin_active = true
	for i in get_children():
		i.call("move", 1)


func stop_icons(delay: float) -> void:
	yield(get_tree().create_timer(delay), "timeout")
	is_spin_active = false
	for i in get_children():
		i.call("slow_down", icon_max_speed)
	yield(get_tree().create_timer(1), "timeout")
	var offset: float = fmod(get_children()[0].rect_position.y, props.h + props.offset_y)
	for i in get_children():
		i.call("normalize_position", offset)


# SIGNALS - - - - - - - - -


# иконка посылает сигнал, когда пересекает черту и потом самоуничтожается
func _on_Icon_imma_head_out(speed: float, position: float) -> void:
	var icon: TextureRect = create_icon(height, death_offset)
	var offset: float
	if direction == UP:
		offset = position + props.h * rows + props.offset_y * (rows - 1)
		icon.set_position(Vector2(0.0, offset))
	elif direction == DOWN:
		offset = position - (props.h * rows + props.offset_y * (rows - 1))
		icon.set_position(Vector2(0.0, offset))
	if is_spin_active:
		icon.call("move", speed)
		icon.call("set_blur", true)
	else:
		icon.call("slow_down", speed)
	# нормализация позиции иконок
	for i in get_children().size():
		var size: int = get_children().size()
		if i + 1 < size:
			if direction == UP:
				get_children()[i + 1].rect_position.y = get_children()[i].rect_position.y + props.h + props.offset_y
			elif direction == DOWN:
				get_children()[i + 1].rect_position.y = get_children()[i].rect_position.y - props.h - props.offset_y
	add_child(icon)
